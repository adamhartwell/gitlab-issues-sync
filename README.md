# gitlab-issues-sync

Synchronize issues from a GitLab project to another.

Using GitLab's Python API, this tools allows migrating  
and syncing issues, milestones and labels automatically.

---

## Usage

```shell
usage: gitlab-issues-sync [-h] [-i INPUT_GITLAB] [-o OUTPUT_GITLAB]
                          [input_project] [output_project] [input_token]
                          [output_token]

gitlab-issues-sync: Synchronize issues from a GitLab project to another
```

| optional arguments   |                                                                                   |
| -------------------- | --------------------------------------------------------------------------------- |
| -h                   | Show this help message                                                            |
| -i INPUT_GITLAB      | Input GitLab URL (default to https://gitlab.com)                                  |
| -o OUTPUT_GITLAB     | Output GitLab URL (default to https://gitlab.com)                                 |

| positional arguments |                                                                                   |
| -------------------- | --------------------------------------------------------------------------------- |
| input_project        | Input project ID number                                                           |
| output_project       | Output project ID number                                                          |
| input_token          | Input project token credential                                                    |
| output_token         | Output project token credential (defaults to output_token)                        |

---

## Dependencies

- [python-gitlab](https://pypi.org/project/python-gitlab/): Interact with GitLab API

---

## References

- [gitlabci-local](https://pypi.org/project/gitlabci-local/): Launch .gitlab-ci.yml jobs locally
- [python-gitlab](https://python-gitlab.readthedocs.io/en/stable/): Documentation for python-gitlab
